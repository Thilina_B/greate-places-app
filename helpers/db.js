import * as SQLite from "expo-sqlite";

const db = SQLite.openDatabase("places.db");

export const init = async () => {
  try {
    const promise = new Promise((resolve, reject) => {
      db.transaction((tx) => {
        tx.executeSql(
          "CREATE TABLE IF NOT EXISTS places (id INTEGER PRIMARY KEY NOT NULL, title TEXT NOT NULL, imageUri TEXT NOT NULL,address TEXT NOT NULL,lat REAL NOT NULL ,lng REAL NOT NULL);",
          [],
          //IF SUCCRESS
          () => {
            resolve();
          },
          //IF FAIL
          (_, err) => {
            reject(err);
          }
        );
      });
    });
    return promise;
  } catch (error) {
    console.log(error);
  }
};

export const insertPlace = async (title, imageUri, address, lat, lng) => {
  try {
    return new Promise((resolve, reject) => {
      db.transaction((tx) => {
        tx.executeSql(
          "INSERT INTO places (title, imageUri, address, lat, lng) VALUES (?, ?, ?, ?, ?);",
          [title, imageUri, address, lat, lng],
          //IF SUCCRESS
          (_, res) => {
            resolve(res);
          },
          //IF FAIL
          (_, err) => {
            reject(err);
          }
        );
      });
    });
  } catch (error) {
    console.log(error);
  }
};

export const fetchPlaces = () => {
  return new Promise((resolve, reject) => {
    db.transaction((tx) => {
      tx.executeSql(
        "SELECT * FROM places",
        [],
        //IF SUCCRESS
        (_, result) => {
          resolve(result);
        },
        //IF FAIL
        (_, err) => {
          reject(err);
        }
      );
    });
  });
};
