import { StatusBar } from "expo-status-bar";
import React from "react";
import { View } from "react-native";
import { applyMiddleware, combineReducers, createStore } from "redux";
import PlaceNavigator from "./navigation/PlaceNavigator";
import places from "./store/reducers/places";
import ReduxThunk from "redux-thunk";
import { Provider } from "react-redux";
import { init } from "./helpers/db";

init().then(() => {
  console.log("Initialize Database");
});

const rootReducer = combineReducers({
  places: places,
});

const store = createStore(rootReducer, applyMiddleware(ReduxThunk));

export default function App() {
  return (
    // <StatusBar style="auto" />
    <Provider store={store}>
      <StatusBar style="auto" />
      <PlaceNavigator />
    </Provider>
  );
}
